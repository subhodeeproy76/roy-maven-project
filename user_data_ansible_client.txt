#!/bin/bash
#Below ssh public key belongs to jenkins user on Jenkins Slave Node which will ssh to client machine & will be be pasted in /home/ec2-user/.ssh/authorized_keys file on the client. So replace it accordingly as per your current agent which you will use. 

hostnamectl set-hostname testvm-dev

mkdir /tmp/myefs

mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport fs-0f3fd4058ab40103f.efs.us-west-2.amazonaws.com:/ /tmp/myefs

echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDbbIxrnkDMLjnWFr6SIw2oDV1TvXZad1iQavStqTMuqoILqtrxajILJiqlkrO+1vacqxx+XKNZjVN7tJtpGruZX+wXo3lyKLAPTy2OCLeVWHuyNfH1nYlbaVpRrF0Cq1ofGjJZBmNBzM/GioOE0HLUpftLO87jr4wGdYelgbtHfc4B8pz11XCt8i3yHETiAD/MsNyEJ3+BT+REjmzVV98Jcbyw/C+zdX+1e+mkhptEwr0iCUumjGXC3r/Ov8h1N8AR1HONzYQJsMBNGhdt2c+h1K4Y+Oc8z7KIyQlmHuORrHDYThTl6hUhbHETaU1qzLoKW7rjLZrBhb7596qXxpzh Jenkins agent key" >> /home/ec2-user/.ssh/authorized_keys
